# modbat-tutorial

This repository contains the Modbat tutorial. Modbat is a model-based tester that can generate thousands of diverse tests from a high-level model.

## Instructions can be found in the [tutorial wiki](https://gitlab.com/cartho/modbat-tutorial/-/wikis/home).

For the tool itself, see: https://gitlab.com/cartho/modbat

The tutorial is designed as a stand-alone project that contains a precompiled version of the Modbat tool (`modbat.jar`).

Requirements: Scala 2.11 or higher (tested on 2.11.12).
